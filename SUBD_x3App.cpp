/***************************************************************
 * Name:      SUBD_x3App.cpp
 * Purpose:   Code for Application Class
 * Author:    Kirill ()
 * Created:   2019-03-23
 * Copyright: Kirill ()
 * License:
 **************************************************************/

#include "SUBD_x3App.h"

//(*AppHeaders
#include "SUBD_x3Main.h"
#include <wx/image.h>
//*)

IMPLEMENT_APP(SUBD_x3App);

bool SUBD_x3App::OnInit()
{
    //(*AppInitialize
    bool wxsOK = true;
    wxInitAllImageHandlers();
    if ( wxsOK )
    {
    	SUBD_x3Frame* Frame = new SUBD_x3Frame(0);
    	Frame->Show();
    	SetTopWindow(Frame);
    }
    //*)
    return wxsOK;

}
