/***************************************************************
 * Name:      SUBD_x3App.h
 * Purpose:   Defines Application Class
 * Author:    Kirill ()
 * Created:   2019-03-23
 * Copyright: Kirill ()
 * License:
 **************************************************************/

#ifndef SUBD_X3APP_H
#define SUBD_X3APP_H

#include <wx/app.h>

class SUBD_x3App : public wxApp
{
    public:
        virtual bool OnInit();
};

#endif // SUBD_X3APP_H
