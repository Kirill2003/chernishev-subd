/***************************************************************
 * Name:      SUBD_x3Main.h
 * Purpose:   Defines Application Frame
 * Author:    Kirill ()
 * Created:   2019-03-23
 * Copyright: Kirill ()
 * License:
 **************************************************************/

#ifndef SUBD_X3MAIN_H
#define SUBD_X3MAIN_H

//(*Headers(SUBD_x3Frame)
#include <wx/frame.h>
#include <wx/menu.h>
#include <wx/statusbr.h>
//*)

class SUBD_x3Frame: public wxFrame
{
    public:

        SUBD_x3Frame(wxWindow* parent,wxWindowID id = -1);
        virtual ~SUBD_x3Frame();

    private:

        //(*Handlers(SUBD_x3Frame)
        void OnQuit(wxCommandEvent& event);
        void OnAbout(wxCommandEvent& event);
        //*)

        //(*Identifiers(SUBD_x3Frame)
        static const long idMenuQuit;
        static const long idMenuAbout;
        static const long ID_STATUSBAR1;
        //*)

        //(*Declarations(SUBD_x3Frame)
        wxStatusBar* StatusBar1;
        //*)

        DECLARE_EVENT_TABLE()
};

#endif // SUBD_X3MAIN_H
